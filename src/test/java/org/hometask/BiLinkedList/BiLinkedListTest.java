package org.hometask.BiLinkedList;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class BiLinkedListTest {
        @Test
        public void ContainTest(){
            BiLinkedList<Integer> list = new BiLinkedList<Integer>();
            list.add(10);
            list.add(20);
            list.add(30);
            list.add(40);

            assertTrue(list.contains(10));
            assertFalse(list.contains(50));
        }

        @Test
    public void ContainAllTest(){
            BiLinkedList<Integer> list = new BiLinkedList<Integer>();
            list.add(10);
            list.add(20);
            list.add(30);
            list.add(40);

            BiLinkedList<Integer> list2 = new BiLinkedList<Integer>();
            list2.add(20);
            list2.add(10);
            list2.add(40);
            list2.add(30);

            assertTrue(list.containsAll(list2));
        }

        @Test
    public void IndexOfTest(){
            BiLinkedList<Integer> list = new BiLinkedList<Integer>();
            list.add(10);
            list.add(20);
            list.add(30);
            list.add(40);

            assertEquals(2,list.indexOf(30));
            assertEquals(0,list.indexOf(80));
        }
    @Test
    public void LastIndexOfTest(){
        BiLinkedList<Integer> list = new BiLinkedList<Integer>();
        list.add(10);
        list.add(20);
        list.add(30);
        list.add(40);
        list.add(20);
        list.add(60);

        assertEquals(4,list.lastIndexOf(20));
    }
    @Test
    public void RemoveAllTest(){
        BiLinkedList<Integer> list = new BiLinkedList<Integer>();
        list.add(10);
        list.add(20);
        list.add(50);
        list.add(30);
        list.add(40);

        BiLinkedList<Integer> list2 = new BiLinkedList<Integer>();
        list2.add(20);
        list2.add(10);
        list2.add(40);
        list2.add(30);

        assertTrue(list.removeAll(list2));

        assertArrayEquals(new Integer[]{50}, list.toArray());
    }

    @Test
    public void AddByIndexTest(){
        BiLinkedList<Integer> list = new BiLinkedList<Integer>();
        list.add(10);
        list.add(20);
        list.add(30);
        list.add(40);

        list.add(2, 7);
        assertArrayEquals(new Integer[]{10,20,7,30,40}, list.toArray());
    }

    @Test
    public void RemoveByIndexTest(){
        BiLinkedList<Integer> list = new BiLinkedList<Integer>();
        list.add(10);
        list.add(20);
        list.add(30);
        list.add(40);
        list.remove(2);
        assertArrayEquals(new Integer[]{10,20,40}, list.toArray());
    }

    @Test
    public void AddAllByIndexTest(){
        BiLinkedList<Integer> list = new BiLinkedList<Integer>();
        list.add(10);
        list.add(20);
        list.add(30);
        list.add(40);

        BiLinkedList<Integer> list2 = new BiLinkedList<Integer>();
        list2.add(1);
        list2.add(2);
        list2.add(3);
        list2.add(4);

        list.addAll(2,list2);
        assertArrayEquals(new Integer[]{10,20,1,2,3,4,30,40}, list.toArray());

    }
}