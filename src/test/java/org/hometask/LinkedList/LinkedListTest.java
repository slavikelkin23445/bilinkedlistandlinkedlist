package org.hometask.LinkedList;

import org.hometask.LinkedList.LinkedList;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LinkedListTest {

    @Test
    public void ContainTest(){
        LinkedList<Integer> list = new LinkedList<Integer>();
        list.add(10);
        list.add(20);
        list.add(30);
        list.add(40);

        assertTrue(list.contains(10));
        assertFalse(list.contains(50));
    }

    @Test
    public void ContainAllTest(){
        LinkedList<Integer> list = new LinkedList<Integer>();
        list.add(10);
        list.add(20);
        list.add(30);
        list.add(40);

        LinkedList<Integer> list2 = new LinkedList<Integer>();
        list2.add(20);
        list2.add(10);
        list2.add(40);
        list2.add(30);

        assertTrue(list.containsAll(list2));
    }

    @Test
    public void IndexOfTest(){
        LinkedList<Integer> list = new LinkedList<Integer>();
        list.add(10);
        list.add(20);
        list.add(30);
        list.add(40);

        assertEquals(2,list.indexOf(30));
        assertEquals(0,list.indexOf(80));
    }
    @Test
    public void LastIndexOfTest(){
        LinkedList<Integer> list = new LinkedList<Integer>();
        list.add(10);
        list.add(20);
        list.add(30);
        list.add(40);
        list.add(20);
        list.add(60);

        assertEquals(4,list.lastIndexOf(20));
    }
    @Test
    public void RemoveAllTest(){
        LinkedList<Integer> list = new LinkedList<Integer>();
        list.add(10);
        list.add(20);
        list.add(50);
        list.add(30);
        list.add(40);

        LinkedList<Integer> list2 = new LinkedList<Integer>();
        list2.add(20);
        list2.add(10);
        list2.add(40);
        list2.add(30);

        assertTrue(list.removeAll(list2));

        assertArrayEquals(new Integer[]{50}, list.toArray());
    }

    @Test
    public void AddByIndexTest(){
        LinkedList<Integer> list = new LinkedList<Integer>();
        list.add(10);
        list.add(20);
        list.add(30);
        list.add(40);

        list.add(2, 7);
        assertArrayEquals(new Integer[]{10,20,7,30,40}, list.toArray());
    }

    @Test
    public void RemoveByIndexTest(){
        LinkedList<Integer> list = new LinkedList<Integer>();
        list.add(10);
        list.add(20);
        list.add(30);
        list.add(40);
        list.remove(2);
        assertArrayEquals(new Integer[]{10,20,40}, list.toArray());
    }

    @Test
    public void AddAllByIndexTest(){
        LinkedList<Integer> list = new LinkedList<Integer>();
        list.add(10);
        list.add(20);
        list.add(30);
        list.add(40);

        LinkedList<Integer> list2 = new LinkedList<Integer>();
        list2.add(1);
        list2.add(2);
        list2.add(3);
        list2.add(4);

        list.addAll(2,list2);
        assertArrayEquals(new Integer[]{10,20,1,2,3,4,30,40}, list.toArray());

    }
}