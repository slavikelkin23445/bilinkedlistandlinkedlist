package org.hometask.LinkedList;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.*;

public class LinkedList<T> implements List<T> {
    private int size = 0;
    private Node<T> head;
    private Node<T> tail;

    @Data
    @Accessors(chain = true)
    private static class Node<T>{
        private T value;
        private Node<T> next;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return head == null;
    }

    @Override
    public boolean contains(Object o) {
        for(Node<T> cur = head; cur!= null; cur = cur.next){
            if(Objects.equals(cur.value, o)){
                return true;
            }
        }
        return false;
    }

    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {
            private Node<T> cur = head;
            @Override
            public boolean hasNext() {
                return cur != null;
            }

            @Override
            public T next() {
                T val = cur.getValue();
                cur = cur.getNext();
                return val;
            }
        };
    }

    @Override
    public Object[] toArray() {
        return toArray(new Object[0]);
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        a = Arrays.copyOf(a, size);
        int i = 0;
        for (T t : this) {
            a[i++] = (T1)t;
        }
        return a;
    }

    @Override
    public boolean add(T t) {
        if(head == null){
            head = tail = new Node<T>().setValue(t);
        }else{
            Node<T> node = new Node<T>().setValue(t).setNext(null);
            tail.setNext(node);
            tail = node;
        }
        size++;
        return true;
    }

    private void removeNode(Node<T> cur){
        if (cur.next != null){
            cur.setValue(cur.next.value).setNext(cur.next.next);
            size--;
        }
        if (cur == tail) removeLastNode();
    }

    private void removeLastNode(){
        Node<T> cur = head;
        while(cur.next.next != null){
            cur = cur.next;
        }
        cur.next = null;
        tail = cur;
        size--;
    }

    @Override
    public boolean remove(Object o) {
        for(Node<T> cur = head; cur!= null; cur = cur.next){
            if(Objects.equals(cur.value, o)){
                removeNode(cur);
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        for (Object el:c) {
            if(contains(el)){
                continue;
            }else{
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        c.forEach(this::add);
        return true;
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        T[] arr = (T[]) c.toArray();
        for(int i = 0; i < c.size(); i++, index++){
            add(index,arr[i]);
        }
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        for (Object el: c) {
            remove(el);
        }
        return true;
    }

    public boolean containsListItem(T val, Collection<?> c){
        for (Object el : c) {
            if (val == el) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        for(Node<T> cur = head; cur!= null; cur = cur.next) {
            if (containsListItem(cur.value, c)) {
                continue;
            }else{
                removeNode(cur);
            }
        }
        return false;
    }

    @Override
    public void clear() {
        head = tail = null;
        size = 0;
    }

    public void checkIndex(int index){
        if(index >= size || index < 0) throw new IndexOutOfBoundsException(index);
    }

    @Override
    public T get(int index) {
        checkIndex(index);
        Node<T> cur = head;
        for (int i = 0; i < index; i++) {
            cur = cur.getNext();
        }
        return cur.getValue();
    }

    @Override
    public T set(int index, T element) {
        checkIndex(index);
        Node<T> cur = head;
        for (int i = 0; i < index; i++) {
            cur = cur.getNext();
        }
        cur.setValue(element);
        return element;
    }

    @Override
    public void add(int index, T element) {
        Node<T> cur = head;
        Node<T> newNode = new Node<T>().setValue(element);

        if(index == 0){
            newNode.next = head;
            head = newNode;
        }else{
            for (int i = 0; i < index - 1; i++) {
                cur = cur.next;
            }
            newNode.next = cur.next;
            cur.next = newNode;
        }
        size++;
    }

    @Override
    public T remove(int index) {
        checkIndex(index);
        Node<T> cur = head;
        for(int i = 0; i<index; i++){
            cur = cur.next;
        }
        removeNode(cur);
        return cur.value;
    }

    @Override
    public int indexOf(Object o) {
        int i = 0;
        if(contains(o)){
            for(Node<T> cur = head; cur!= null; cur = cur.next, i++){
                if(Objects.equals(cur.value,o)) return i;
            }
        }
        return i;
    }

    @Override
    public int lastIndexOf(Object o) {
        T[] arr = (T[])this.toArray();
        for(int i = size-1; i > 0; i--){
            if(Objects.equals(arr[i], o)) return i;
        }
        return 0;
    }

    @Override
    public ListIterator<T> listIterator() {
        return null;
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        return null;
    }

    public void checkIndex(int fromIndex, int toIndex){
        if(fromIndex >= size || fromIndex < 0) throw new IndexOutOfBoundsException(fromIndex);
        else if(toIndex >= size+1 || toIndex < 0) throw new IndexOutOfBoundsException(toIndex);
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        checkIndex(fromIndex, toIndex);

        List<T> subList = new LinkedList<>();
        Object[] tempArr = this.toArray();

        for(int i = fromIndex; i < toIndex; i++){
            subList.add((T)tempArr[i]);
        }
        return subList;
    }
}
