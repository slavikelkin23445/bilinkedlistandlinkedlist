package org.hometask.BiLinkedList;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.*;
import java.util.function.Consumer;

public class BiLinkedList<T> implements List<T> {
    private int size = 0;
    private Node<T> head;
    private Node<T> tail;

    @Data
    @Accessors(chain = true)
    private static class Node<T>{
       private T value;
       private Node<T> next;
       private Node<T> prev;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return head == null;
    }

    @Override
    public boolean contains(Object o) {
        for(Node<T>cur = head; cur!= null; cur = cur.next){
            if(Objects.equals(cur.value, o)){
                return true;
            }
        }
        return false;
    }

    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {
           private Node<T> cur = head;
            @Override
            public boolean hasNext() {
                return cur != null;
            }

            @Override
            public T next() {
                T val = cur.getValue();
                cur = cur.getNext();
                return val;
            }
            @Override
            public void remove(){
                removeNode(cur);
            }
        };
    }

    public Iterator<T> reversedIterator(){
        return new Iterator<T>() {
           private Node<T> cur = tail;
            @Override
            public boolean hasNext() {
                return cur != null;
            }

            @Override
            public T next() {
                T val = cur.getValue();
                cur = cur.getPrev();
                return val;
            }
            @Override
            public void forEachRemaining(Consumer action) {
                Iterator.super.forEachRemaining(action);
            }
        };
    }

    @Override
    public Object[] toArray() {
        return toArray(new Object[0]);
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        a = Arrays.copyOf(a, size);
        int i = 0;
        for (T t : this) {
            a[i++] = (T1)t;
        }
        return a;
    }

    @Override
    public boolean add(T t) {
        if(head == null){
            head = tail = new Node<T>().setValue(t);
        }else{
            var node = new Node<T>().setValue(t).setPrev(tail);
            tail.setNext(node);
            tail = node;
        }
        size++;
        return true;
    }

    private void removeNode(Node<T> cur){
        if(cur == head && cur == tail){
            head=tail=null;
            size = 0;
        }else {
            if (cur.prev != null) cur.prev.setNext(cur.next);
            if (cur.next != null) cur.next.setPrev(cur.prev);

            if (cur == head)head = cur.next;
            else if (cur == tail)tail = cur.prev;
            size--;
        }
    }
    @Override
    public boolean remove(Object o) {
        for(Node<T>cur = head; cur!= null; cur = cur.next){
            if(Objects.equals(cur.value, o)){
                removeNode(cur);
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        for (Object el:c) {
            if(contains(el)){
                continue;
            }else{
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        c.forEach(this::add);
        return true;
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        T[] arr = (T[]) c.toArray();
        for(int i = 0; i < c.size(); i++, index++){
            add(index,arr[i]);
        }
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        for (Object el: c) {
            remove(el);
        }
        return true;
    }

    public boolean containsListItem(T val, Collection<?> c){
        for (Object el : c) {
            if (val == el) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        for(Node<T>cur = head; cur!= null; cur = cur.next) {
            if (containsListItem(cur.value, c)) {
                continue;
            } else {
                removeNode(cur);
            }
        }
        return true;
    }

    @Override
    public void clear() {
        head = tail = null;
        size = 0;
    }

    public void checkIndex(int index){
        if(index >= size || index < 0) throw new IndexOutOfBoundsException(index);
    }

    @Override
    public T get(int index) {
        checkIndex(index);
        Node<T> cur = head;
        for (int i = 0; i < index; i++) {
            cur = cur.getNext();
        }
        return cur.getValue();
    }

    @Override
    public T set(int index, T element) {
        checkIndex(index);
        Node<T> cur = head;
        for (int i = 0; i < index; i++) {
            cur = cur.getNext();
        }
        cur.setValue(element);
        return element;
    }

    @Override
    public void add(int index, T element) {
        checkIndex(index);
        Node<T> cur = head;
        Node<T> newNode = new Node<T>().setValue(element);
        for(int i = 0; i<index-1; i++){
            cur = cur.next;
        }
        newNode.next = cur.next;
        cur.next = newNode;
        size++;
    }

    @Override
    public T remove(int index) {
        checkIndex(index);
        Node<T> cur = head;
        for(int i = 0; i<index; i++){
            cur = cur.next;
        }
        removeNode(cur);
        return cur.value;
    }

    @Override
    public int indexOf(Object o) {
        int i = 0;
        if(contains(o)){
            for(Node<T>cur = head; cur!= null; cur = cur.next, i++){
                if(Objects.equals(cur.value,o)) return i;
            }
        }
        return i;
    }

    @Override
    public int lastIndexOf(Object o) {
        int i = 0;
        if(contains(o)){
            for(Node<T>cur = tail; cur != head; cur = cur.prev, i++){
                if(Objects.equals(cur.value,o)) return size - 1 - i;
            }
        }
        return i;
    }

    @Override
    public ListIterator<T> listIterator() {
        return new ListIterator<T>() {
           private Node<T> cur = head;
           private Node<T> prev = tail;
           private int indexNext = 0;
           private int indexPrev = size-1;
            @Override
            public boolean hasNext() {
                return cur!=null;
            }

            @Override
            public T next() {
                T val = cur.getValue();
                cur = cur.getNext();
                return val;
            }

            @Override
            public boolean hasPrevious() {
                return prev != null;
            }

            @Override
            public T previous() {
                T val = prev.getValue();
                prev = prev.getPrev();
                return val;
            }

            @Override
            public int nextIndex() {
                if(indexNext > size-1) return size-1;
                return indexNext++;
            }

            @Override
            public int previousIndex() {
                if(indexPrev < 0) return 0;
                return indexPrev--;
            }

            @Override
            public void remove() {
                removeNode(cur);
            }

            @Override
            public void set(T t) {
                cur.setValue(t);
            }

            @Override
            public void add(T t) {
                if(cur == null){
                    cur = prev = new Node<T>().setValue(t);
                }else{
                    var node = new Node<T>().setValue(t).setPrev(prev);
                    prev.setNext(node);
                    prev = node;
                }
                size++;
            }
        };
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        return null;
    }

    public void checkIndex(int fromIndex, int toIndex){
        if(fromIndex >= size || fromIndex < 0) throw new IndexOutOfBoundsException(fromIndex);
        else if(toIndex >= size+1 || toIndex < 0) throw new IndexOutOfBoundsException(toIndex);
    }


    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        checkIndex(fromIndex, toIndex);

        List<T> subList = new BiLinkedList<>();
        Object[] tempArr = this.toArray();

        for(int i = fromIndex; i < toIndex; i++){
            subList.add((T)tempArr[i]);
        }
        return subList;
    }
}
